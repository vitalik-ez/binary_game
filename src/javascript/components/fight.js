import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', function(event) {
	  if (event.code == 'KeyA') {
	    getDamage(firstFighter, secondFighter);
	  } else if (event.code == 'KeyJ') {
	  	getDamage(secondFighter, firstFighter);
	  }
	  if (firstFighter.health <= 0) {
	  	resolve(secondFighter.name);
	  }
	  if (secondFighter.health <= 0) {
	  	resolve(firstFighter.name);
	  }
	  console.log('health first', firstFighter.health, ' second ', secondFighter.health);
	});
	
    //setTimeout(() => resolve("result"), 2000)
  });
}

export function getDamage(attacker, defender) {
	defender.health -= attacker.attack;
}

export function getHitPower(fighter) {
  // return hit power
}

export function getBlockPower(fighter) {
  // return block power
}
