import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if ( fighter !== undefined ) {
    fighterElement.appendChild(createFighterImage(fighter, positionClassName));
  }
  return fighterElement;
}

export function createFighterImage(fighter, positionClassName) {
  console.log(fighter);
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    //className: 'fighter-preview___img',
    className: positionClassName,
    attributes,
  });

  return imgElement;
}
